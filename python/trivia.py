#!/usr/bin/env python

class Players:
    def __init__(self):
        self.players = []
        self._current_player_index = 0
    
    def add(self, player):
        self.players.append(player)

    def next(self):
        if self._current_player_index >= self.count - 1:
            self._current_player_index = 0
        else:
            self._current_player_index += 1

    @property
    def current(self):
        return self.players[self._current_player_index]

    @property
    def count(self):
        return len(self.players)

class Player:
    def __init__(self, player_name):
        self.name = player_name
        self.purse = 0
        self.place = 0
        self.is_in_penalty_box = False

    def win_gold_coin(self):
        self.purse += 1

    def move(self, roll):
        self.place += roll
        if self.place > 11:
            self.place -= 12

    def send_to_penalty_box(self):
        self.is_in_penalty_box = True;           

class Questions:
    def __init__(self):
        self.categories = ["Pop", "Science", "Sports", "Rock"]
        self.questions = {}

        for category in self.categories:
            self.questions[category] = []
            for i in range(50):
                question = "{0} Question {1}".format(category, i)
                self.questions[category].append(question)
            
    def draw_a_question(self, category):
        question_category_stack = self.questions[category];
        question = question_category_stack.pop(0)
        return question
        
class Board:
    @staticmethod
    def get_question_category(place):
        category_index = place % 4
        if category_index == 0: 
            return 'Pop'
        if category_index == 1:
            return 'Science'
        if category_index == 2:
            return 'Sports'
        return 'Rock'    

class Game:

    def __init__(self, 
        player1, player2,
        player3 = None, player4 = None, 
        player5 = None, player6 = None):
       
        self.players = Players()
        self._add_players(
            [player1, player2,
             player3, player4,
             player5, player6])
              
        self.questions = Questions()
        self.is_getting_out_of_penalty_box = False
             
    def _add_players(self, players):
        for player in players:
            self._add_player(player)

    def _add_player(self, player_name):
        if player_name is None:
            return

        player = Player(player_name)
        self.players.add(player)

        print player.name + " was added"
        print "They are player number %s" % self.players.count
    
    def play(self, roll):
        print "%s is the current player" % self.players.current.name
        print "They have rolled a %s" % roll
        
        if self.players.current.is_in_penalty_box:
            self.is_getting_out_of_penalty_box = self._should_get_out_of_penalty_box(roll)
            if(not self.is_getting_out_of_penalty_box):
                return

        self._move_player(roll)
        self._ask_question()
    
    def _should_get_out_of_penalty_box(self, roll):
        if(roll % 2 == 0):
            print "%s is not getting out of the penalty box" % self.players.current.name
            return False
        else:
            print "%s is getting out of the penalty box" % self.players.current.name 
            return True

    def _move_player(self, roll):
        self.players.current.move(roll)

        print self.players.current.name + \
                    '\'s new location is ' + \
                    str(self.players.current.place)

    def _ask_question(self):
        category = Board.get_question_category(self.players.current.place)
        print "The category is %s" % category
        print self.questions.draw_a_question(category)

    def was_correctly_answered(self):
        if self.players.current.is_in_penalty_box:
            if not self.is_getting_out_of_penalty_box:
                self.players.next()
                return False

        print "Answer was correct!!!!"
        self._increase_player_gold()
        
        game_over = self._is_game_over()
        self.players.next()
        
        return game_over

    def _increase_player_gold(self):
        self.players.current.win_gold_coin()
        print self.players.current.name + \
            ' now has ' + \
            str(self.players.current.purse) + \
            ' Gold Coins.'
    
    def wrong_answer(self):
        print 'Question was incorrectly answered'
        print self.players.current.name + " was sent to the penalty box"
        
        self.players.current.send_to_penalty_box()
        self.players.next()
    
    def _is_game_over(self):
        return self.players.current.purse == 6

from random import randrange

class Runner:
    @staticmethod
    def main():
        game_is_over = False
        game = Game('Chet', 'Pat', 'Sue')

        while not game_is_over:
            roll = Runner._roll_dice();
            game.play(roll)
            answer = Runner._answer()
            if Runner._answer_is_right(answer):
                game_is_over = game.was_correctly_answered()
            else:
                game.wrong_answer()
    
    @staticmethod
    def _roll_dice():
        return randrange(5) + 1

    @staticmethod
    def _answer():
        return randrange(9)

    @staticmethod
    def _answer_is_right(answer):
        return answer != 7


if __name__ == '__main__':
    Runner.main()