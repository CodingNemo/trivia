import unittest

from triviaOld import RunnerOld
from trivia import Runner

from cStringIO import StringIO
import sys

from random import seed

class TriviaGoldenMaster(unittest.TestCase):
  
    def setUp(self):
        self.numberOfSeeds = 100
        self.goldenMaster = self._run_old_trivia()    

    def test_that_new_trivia_still_behaves_as_old_trivia(self):
        actual = self._run_trivia()
        self.assertEqual(self.goldenMaster, actual)

    def _run_old_trivia(self):
        return TriviaGoldenMaster._run_many(RunnerOld.main, self.numberOfSeeds)

    def _run_trivia(self):
        return TriviaGoldenMaster._run_many(Runner.main, self.numberOfSeeds)
    
    @staticmethod
    def _run_many(runner, runCount):
        output = ''
        currentseed = 0
        while currentseed < runCount :
            seed(currentseed)
            output = output + 'SEED : ' + str(currentseed) + '\n'
            mystdout = StringIO()
            sys.stdout = mystdout
            runner()
            sys.stdout = sys.__stdout__
            output = output + mystdout.getvalue()
            mystdout.close()
            output = output + '**********' + '\n'
            currentseed = currentseed + 1
        return output