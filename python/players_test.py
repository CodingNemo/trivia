import unittest

from trivia import Players
from trivia import Player

class PlayersTests(unittest.TestCase):
    
    def test_that_adding_a_player_increases_the_number_of_players(self):
        players = Players()

        players.add(Player("Ernest"))
        
        self.assertEqual(players.count, 1)

    def test_that_adding_another_player_increases_the_number_of_players(self):
        players = Players()

        players.add(Player("Ernest"))
        players.add(Player("John"))
        
        self.assertEqual(players.count, 2)

    def test_that_current_player_should_be_the_first_player_when_starting_enumeration(self):
        players = Players()

        players.add(Player("Ernest"))
        players.add(Player("John"))

        self.assertEqual(players.current.Name, "Ernest")

    
    def test_that_next_player_should_be_the_next_added_player(self):
        players = Players()

        players.add(Player("Ernest"))
        players.add(Player("John"))
        
        players.next()
        
        self.assertEqual(players.current.Name, "John")

    def test_that_next_player_should_be_the_first_added_player_when_current_player_is_the_last_player(self):
        players = Players()

        players.add(Player("Ernest"))
        players.add(Player("John"))
        
        players.next()
        players.next()
        
        self.assertEqual(players.current.Name, "Ernest")